package iam

import (
	"fmt"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/awserr"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/iam"
)

type IamGroup struct {
	Exists      bool
	Group       *iam.Group
	IsTruncated *bool
	Users       []string
}

type IamGroupPolicy *iam.GetGroupPolicyOutput

// https://docs.aws.amazon.com/sdk-for-go/api/service/iam/#Group
func Group(groupName string) IamGroup {
	svc := iam.New(session.New())
	input := &iam.GetGroupInput{
		GroupName: aws.String(groupName),
	}

	var exists bool

	result, err := svc.GetGroup(input)
	if err != nil {
		if aerr, ok := err.(awserr.Error); ok {
			switch aerr.Code() {
			case iam.ErrCodeNoSuchEntityException:
				fmt.Println(iam.ErrCodeNoSuchEntityException, aerr.Error())
			case iam.ErrCodeServiceFailureException:
				fmt.Println(iam.ErrCodeServiceFailureException, aerr.Error())
			default:
				fmt.Println(aerr.Error())
			}
		} else {
			fmt.Println(err.Error())
		}
	}

	if *result.Group.GroupName != "" {
		exists = true
	} else {
		exists = false
	}

	var usernames []string
	for _, user := range result.Users {
		usernames = append(usernames, *user.UserName)
	}

	return IamGroup{
		Exists:      exists,
		Group:       result.Group,
		IsTruncated: result.IsTruncated,
		Users:       usernames,
	}
}

// https://docs.aws.amazon.com/sdk-for-go/api/service/iam/#GetGroupPolicyOutput
func GroupPolicy(groupName string, policyName string) *iam.GetGroupPolicyOutput {
	svc := iam.New(session.New())
	input := &iam.GetGroupPolicyInput{
		GroupName:  aws.String(groupName),
		PolicyName: aws.String(policyName),
	}

	result, err := svc.GetGroupPolicy(input)
	if err != nil {
		if aerr, ok := err.(awserr.Error); ok {
			switch aerr.Code() {
			case iam.ErrCodeNoSuchEntityException:
				fmt.Println(iam.ErrCodeNoSuchEntityException, aerr.Error())
			case iam.ErrCodeServiceFailureException:
				fmt.Println(iam.ErrCodeServiceFailureException, aerr.Error())
			default:
				fmt.Println(aerr.Error())
			}
		} else {
			fmt.Println(err.Error())
		}
	}

	return result
}

func Users(maxItems ...int64) []string {
	svc := iam.New(session.New())

	if maxItems == nil {
		maxItems = []int64{100}
	}

	input := &iam.ListUsersInput{
		MaxItems: &maxItems[0],
	}

	result, err := svc.ListUsers(input)
	if err != nil {
		if aerr, ok := err.(awserr.Error); ok {
			switch aerr.Code() {
			case iam.ErrCodeServiceFailureException:
				fmt.Println(iam.ErrCodeServiceFailureException, aerr.Error())
			default:
				fmt.Println(aerr.Error())
			}
		} else {
			fmt.Println(err.Error())
		}
	}

	var usernames []string
	for _, user := range result.Users {
		usernames = append(usernames, *user.UserName)
	}

	return usernames
}

type IamUser struct {
	Exists bool
	User   *iam.User
}

// https://docs.aws.amazon.com/sdk-for-go/api/service/iam/#User
func User(userName string) IamUser {
	svc := iam.New(session.New())
	input := &iam.GetUserInput{
		UserName: aws.String(userName),
	}

	var exists bool

	result, err := svc.GetUser(input)
	if err != nil {
		if aerr, ok := err.(awserr.Error); ok {
			switch aerr.Code() {
			case iam.ErrCodeNoSuchEntityException:
				fmt.Println(iam.ErrCodeNoSuchEntityException, aerr.Error())
			case iam.ErrCodeServiceFailureException:
				fmt.Println(iam.ErrCodeServiceFailureException, aerr.Error())
			default:
				fmt.Println(aerr.Error())
			}
		} else {
			fmt.Println(err.Error())
		}
	}

	if *result.User.UserName != "" {
		exists = true
	} else {
		exists = false
	}

	return IamUser{
		Exists: exists,
		User:   result.User,
	}
}
