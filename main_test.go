package main_test

import (
	"testing"

	"./aws/iam"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
)

func TestIAM(t *testing.T) {
	RegisterFailHandler(Fail)
	RunSpecs(t, "IAM")
}

var _ = Describe("IAM All Users", func() {

	It("users", func() {
		users := iam.Users()

		Expect(users).To(Not(BeEmpty()))
		Expect(len(users)).To(Equal(32))
		Expect(users).To(ContainElement("julien.levasseur"))
	})
})

var _ = Describe("IAM Group", func() {

	It("all-users group", func() {
		group := iam.Group("all-users")

		Expect(group.Exists).To(BeTrue())
		Expect(*group.Group.GroupName).To(Equal("all-users"))
		Expect(len(group.Users)).To(Equal(25))
		Expect(group.Users).To(ContainElement("julien.levasseur"))
	})
})

var _ = Describe("IAM User", func() {

	It("julien.levasseur user", func() {
		user := iam.User("julien.levasseur")

		Expect(user.Exists).To(BeTrue())
	})
})
