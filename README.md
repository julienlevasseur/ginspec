# Ginspec

## IAM Group test example

```go
var _ = Describe("IAM Group", func() {
    var (
        group iam.IamGroup
    )

    BeforeEach(func() {
        group = iam.Group("all-users")
    })

    Describe("Checking group name", func() {
        It("should be", func() {
            Expect(*group.Group.GroupName).To(Equal("all-users"))
        })
    })

    Describe("Checking group users number", func() {
        It("should be", func() {
            Expect(len(group.Users)).To(Equal(25))
        })
    })
})
```

or

```go
func TestIAM(t *testing.T) {
    RegisterFailHandler(Fail)
    RunSpecs(t, "IAM")
}

var _ = Describe("IAM All Users", func() {

    It("users", func() {
        users := iam.Users()

        Expect(users).To(Not(BeEmpty()))
        Expect(len(users)).To(Equal(32))
        Expect(users).To(ContainElement("julien.levasseur"))
    })
})

var _ = Describe("IAM Group", func() {

    It("all-users group", func() {
        group := iam.Group("all-users")

        Expect(group.Exists).To(BeTrue())
        Expect(*group.Group.GroupName).To(Equal("all-users"))
        Expect(len(group.Users)).To(Equal(25))
        Expect(group.Users).To(ContainElement("julien.levasseur"))
    })
})

var _ = Describe("IAM User", func() {

    It("julien.levasseur user", func() {
        user := iam.User("julien.levasseur")

        Expect(user.Exists).To(BeTrue())
    })
})
```
